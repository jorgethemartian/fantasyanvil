@extends('basic')

@section('content')
<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
  @include('errors.list')
  <form role="form" method="POST" action="/users/account/register" class="form-layout">
    {!! csrf_field() !!}
    <div class="text-center mb15">
      <h1>FantasyAnvil</h1>
    </div>
    <div class="form-inputs">
      <input value="{{ old('name') }}" name="name" type="text" class="form-control input-lg" placeholder="Name" autofocus>
    </div>
    <div class="form-inputs">
      <input value="{{ old('email') }}" name="email" type="email" class="form-control input-lg" placeholder="Email address" autofocus>
    </div>
    <div class="form-inputs">
      <input type="password" name="password" class="form-control input-lg" placeholder="Password">
    </div>
    <div class="form-inputs">
      <input type="password" name="password_confirmation" class="form-control input-lg" placeholder="Confirm Password">
    </div>
    <div>
    <button class="btn btn-success btn-lg btn-block" type="submit">Register</button>
    </div>
  </form>
</div>
@stop