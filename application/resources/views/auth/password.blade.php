@extends('basic')

  @section('content')
    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
      @include('errors.list')
      <form role="form" method="POST" action="/users/account/recovery" class="form-layout">
          {!! csrf_field() !!}
        <div class="text-center mb15">
          <h1>FantasyAnvil</h1>
        </div>
        <p class="text-center mb25">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>
        <div class="form-inputs">
          <input value="{{ old('email') }}" name="email" type="email" class="form-control input-lg" placeholder="Email address" autofocus>
        </div>
        <button class="btn btn-success btn-lg btn-block" type="submit">Send Password Reset Link</button>
      </form>
    </div>
  @stop