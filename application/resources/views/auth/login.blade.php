@extends('basic')

  @section('content')
    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
      @include('errors.list')
      <form role="form" method="POST" action="/users/account/login" class="form-layout">
        {!! csrf_field() !!}
        <div class="text-center mb15">
          <h1>FantasyAnvil
        </div>
        <p class="text-center mb30">Welcome to FantasyAnvil. Please sign in to your account</p>
        <div class="form-inputs">
          <input type="email" class="form-control input-lg" placeholder="Email Address" name="email" value="{{ old('email') }}">
          <input type="password" class="form-control input-lg" placeholder="Password" name="password" id="password">
        </div>
        <div>
          <input type="checkbox" name="remember"> Remember Me
        </div class="form-inputs">
        <button class="btn btn-success btn-block btn-lg mb15" type="submit">
          <span>Sign in</span>
        </button>
        <p>
          <a href="/users/account/register">Register for an account</a> ·
          <a href="/users/account/recovery">Forgot your password?</a>
        </p>
      </form>
    </div>
  @stop