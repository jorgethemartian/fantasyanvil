@include('includes.basic-head')
  <div class="app layout-fixed-header bg-white usersession">
    <div class="full-height">
      <div class="center-wrapper">
        <div class="center-content">
          <div class="row no-margin">
            @yield('content')
          </div>
        </div>
      </div>
    </div>
  </div>
@include('includes.basic-footer')