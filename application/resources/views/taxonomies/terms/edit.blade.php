@if ($formType === 'Add Term')
  {!! Form::open(['method' => 'POST', 'action' => ['VocabulariesTermsController@store', $term->vocabulary_id]]) !!}
@else
  {!! Form::model($term, ['method' => 'PUT', 'action' => ['VocabulariesTermsController@update', $term->vocabulary_id, $term->id]]) !!}
@endif
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  <h4 class="modal-title">{{ $formType }}</h4>
</div>
<div class="modal-body">
  <div class="form-group">
    {!! Form::hidden('vocabulary_id', $term->vocabulary_id) !!}
    {!! Form::label('term', 'Term') !!}
    {!! Form::text('term', $term->term, array('class' => 'form-control')) !!}
  </div>
</div>
<div class="modal-footer no-border">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  {!! Form::button('Send', array('class' => 'btn btn-primary', 'type' => 'submit')) !!}
</div>
{!! Form::close() !!}
