@extends('system-template')
@section('content')

<div class="panel mb25">
  <div class="panel-heading">
    <h2>Terms</h2>
  </div>

  <div class="panel-body">
    <div class="pull-left">
      <p>
        {{ $terms->count() }} of {{ $terms->total() }} terms showing |
          show per page
          <a href="{{ Request::url() }}?count=10">10</a>
          <a href="{{ Request::url() }}?count=25">25</a>
          <a href="{{ Request::url() }}?count=50">50</a>
      </p>

      @if (strpos(Request::getQueryString(),'count') !== false)
        {!! $terms->appends(['count' => Input::get('count')])->render() !!}
      @else
        {!! $terms->render() !!}
      @endif
    </div>
    <div class="pull-right">
      <a
            href="{{ action('VocabulariesTermsController@create', ['id' => $vocabulariesId]) }}"
            class="btn btn-success pull-right btn-icon add"
            data-toggle="modal"
            data-target=".add-vocabulary"><i class="fa fa-plus-square mr5"></i> Add term</a>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Terms</th>
          <th></th>
      </thead>
      <tbody>
        @foreach($terms as $term)
          <tr>
            <td>
              <a
                  href="{{ action('VocabulariesTermsController@edit', ['vocabularyId' => $vocabulariesId, 'termId' => $term->id]) }}"
                  data-toggle="modal"
                  data-target=".add-vocabulary">{{ $term->term }}</a></td>
            <td>
              <button
                type="button"
                class="btn btn-danger btn-icon pull-right swal-warning-confirm"
                data-url="{{ action('VocabulariesTermsController@destroy', ['vocabularyId' => $vocabulariesId, 'termId' => $term->id]) }}"
                data-token="{{ csrf_token() }}"
                data-title-warning="Are you sure you wish to delete {{ $term->term }} term?"
                data-title-success="Deleted {{ $term->term }} term."
                data-message-warning="Once you delete this term it cannot be undone!"
                data-message-success="Term {{ $term->term }} has been deleted.">
                <i class="fa fa-minus-square mr5"></i> Delete Terms
              </button>
              <a
                  href="{{ action('VocabulariesTermsController@edit', ['vocabularyId' => $vocabulariesId, 'termId' => $term->id]) }}"
                  class="btn btn-info pull-right btn-icon"
                  data-toggle="modal"
                  data-target=".add-vocabulary"
                  style="margin-right:10px;">
                <i class="fa fa-pencil-square mr5"></i> Edit term</a></td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@stop

@section('stylesheet')
<link rel="stylesheet" href="/vendor/sweetalert/lib/sweet-alert.css">
<style>
  .bg-info {
    padding:15px;
  }
  .add {
    margin-right:16px;
  }
</style>
@stop

@section('javascript')
<script src="/vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="/scripts/alert.js"></script>
<div class="modal add-vocabulary" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
@stop
