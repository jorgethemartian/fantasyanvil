@if (!isset($vocabulary->id))
  {!! Form::open(['method' => 'POST', 'action' => ['VocabulariesController@store']]) !!}
@else
  {!! Form::model($vocabulary, ['method' => 'PATCH', 'action' => ['VocabulariesController@update', $vocabulary->id]]) !!}
@endif
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  <h4 class="modal-title">{{ $formType }}</h4>
</div>
<div class="modal-body">
  <div class="form-group">
    {!! Form::label('vocabulary', 'Vocabulary') !!}
    {!! Form::text('vocabulary', $vocabulary->name, array('class' => 'form-control')) !!}
  </div>
</div>
<div class="modal-footer no-border">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  {!! Form::button('Send', array('class' => 'btn btn-primary', 'type' => 'submit')) !!}
</div>
{!! Form::close() !!}
