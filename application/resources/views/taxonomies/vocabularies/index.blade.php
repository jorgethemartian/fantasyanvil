@extends('system-template')
@section('content')

<div class="panel mb25">
  <div class="panel-heading">
    <h2>Vocabularies</h2>
  </div>

  <div class="panel-body">
    <div class="pull-left">
      <p>
        {{ $vocabularies->count() }} of {{ $vocabularies->total() }} vocabularies showing |
          show per page
          <a href="{{ Request::url() }}?count=10">10</a>
          <a href="{{ Request::url() }}?count=25">25</a>
          <a href="{{ Request::url() }}?count=50">50</a>
      </p>

      @if (strpos(Request::getQueryString(),'count') !== false)
        {!! $vocabularies->appends(['count' => Input::get('count')])->render() !!}
      @else
        {!! $vocabularies->render() !!}
      @endif
    </div>
    <div class="pull-right">
      <a
            href="{{ action('VocabulariesController@create') }}"
            class="btn btn-success pull-right btn-icon add"
            data-toggle="modal"
            data-target=".add-vocabulary"><i class="fa fa-plus-square mr5"></i> Add vocabulary</a>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Vocabulary</th>
          <th></th>
      </thead>
      <tbody>
        @foreach($vocabularies as $vocabulary)
          <tr>
            <td><a
                  href="{{ action('VocabulariesController@edit', ['id' => $vocabulary->id]) }}"
                  data-toggle="modal"
                  data-target=".add-vocabulary">{{ $vocabulary->vocabulary }}</a></td>
            <td>
                <button
                  type="button"
                  class="btn btn-danger btn-icon pull-right swal-warning-confirm"
                  data-url="{{ action('VocabulariesController@destroy', ['id' => $vocabulary->id]) }}"
                  data-token="{{ csrf_token() }}"
                  data-title-warning="Are you sure you wish to delete {{ $vocabulary->vocabulary }} vocabulary?"
                  data-title-success="Deleted {{ $vocabulary->vocabulary }} vocabulary."
                  data-message-warning="Once you delete this vocabulary it cannot be undone!"
                  data-message-success="Vocabulary {{ $vocabulary->vocabulary }} has been deleted.">
                  <i class="fa fa-minus-square mr5"></i> Delete Terms
                </button>
                <a
                  href="{{ action('VocabulariesController@edit', ['id' => $vocabulary->id]) }}"
                  class="btn btn-info pull-right btn-icon"
                  data-toggle="modal"
                  data-target=".add-vocabulary"
                  style="margin-right:10px;"><i class="fa fa-pencil-square mr5"></i> Edit vocabulary</a>
                <a
                  href="{{ action('VocabulariesTermsController@index', ['id' => $vocabulary->id]) }}"
                  class="btn btn-warning pull-right btn-icon"
                  style="margin-right:10px;"><i class="fa fa-plus-square mr5"></i> Add Terms</a>&nbsp;</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@stop

@section('stylesheet')
<link rel="stylesheet" href="/vendor/sweetalert/lib/sweet-alert.css">
<style>
  .bg-info {
    padding:15px;
  }
  .add {
    margin-right:16px;
  }
</style>
@stop

@section('javascript')
<script src="/vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="/scripts/alert.js"></script>
<div class="modal add-vocabulary" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
@stop
