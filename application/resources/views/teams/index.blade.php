@extends('system-template')

@section('content')

<div class="panel mb25">
  <div class="panel-heading">
    <h2>NFL Teams</h2>
  </div>

  <div class="panel-body">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Code</th>
          <th>Team</th>
      </thead>
      <tbody>
        @foreach($teams as $team)
          <tr>
            <td>{{ $team->code }}</td>
            <td>{{ $team->full_name }}</td>
            <td><a href="{{ url('admin/fantasy/nfl-teams/' . strtolower($team->code) . '/players') }}" class="btn btn-info pull-right">View team players</a></td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@stop

@section('stylesheet')
@stop

@section('javascript')
@stop
