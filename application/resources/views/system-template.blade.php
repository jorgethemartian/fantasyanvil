@include('includes.basic-head')
@include('includes.quicklaunch')
<div class="app layout-fixed-header">

  <!-- sidebar panel -->
  <div class="sidebar-panel offscreen-left">

    <div class="brand">

      <!-- logo -->
      <div class="brand-logo">
        <img src="/images/logo.png" height="15" alt="">
      </div>
      <!-- /logo -->

      <!-- toggle small sidebar menu -->
      <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </a>
      <!-- /toggle small sidebar menu -->

    </div>

    @include('includes.navigation')

  </div>
  <!-- /sidebar panel -->

  <!-- content panel -->
  <div class="main-panel">
  @include('includes.top-header')

    <!-- main area -->
    <div class="main-content">
      @yield('content')
    </div>
    <!-- /main area -->
  </div>
  <!-- /content panel -->

  @include('includes.bottom-footer')

  @include('includes.rightsidebar')
</div>
@include('includes.basic-footer')
