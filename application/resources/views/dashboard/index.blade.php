@extends('system-template')

@section('content')

<div class="panel mb25">
  <div class="panel-heading">
    <h2>Dashboard</h2>
  </div>

  <div class="panel-body">
    <a href="{{ action('TwitterController@login') }}" class="btn btn-icon btn-primary"><i class="fa fa-twitter mr5"></i>Sign in to Twitter</a>
  </div>
</div>
@stop

@section('stylesheet')
@stop

@section('javascript')
@stop
