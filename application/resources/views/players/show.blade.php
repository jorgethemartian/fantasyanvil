@extends('system-template')

@section('content')
<div class="col-md-12">
  <h2>{{ $player['info'][0]->display_name }}</h2>
  <hr>
</div>
<div class="col-md-12">
  <h3>Tweets</h3>
  <p>
    {{ $player['tweets']->count() }} of {{ $player['tweets']->total() }} players showing |
      show per page
      <a href="{{ Request::url() }}?count=10">10</a>
      <a href="{{ Request::url() }}?count=25">25</a>
      <a href="{{ Request::url() }}?count=50">50</a>
  </p>
  @if (strpos(Request::getQueryString(),'count') !== false)
    {!! $player['tweets']->appends(['count' => Input::get('count')])->render() !!}
  @else
    {!! $player['tweets']->render() !!}
  @endif
  <section class="widget bg-white post-comments">
    @foreach ($player['tweets'] as $tweet)
      <div class="media">
        <div class="comment">
          <div class="comment-author h6 no-margin">
            <div class="comment-meta small">{{ $tweet->tweet_created_at }}</div>
            <h5><strong>{!! $tweet->twitter_screen_name !!}</strong></h5>
          </div>
          <p>{!! $tweet->text !!}</p>
        </div>
      </div>
      <hr>
    @endforeach
  </section>
  <p>
    {{ $player['tweets']->count() }} of {{ $player['tweets']->total() }} players showing |
      show per page
      <a href="{{ Request::url() }}?count=10">10</a>
      <a href="{{ Request::url() }}?count=25">25</a>
      <a href="{{ Request::url() }}?count=50">50</a>
  </p>
  @if (strpos(Request::getQueryString(),'count') !== false)
    {!! $player['tweets']->appends(['count' => Input::get('count')])->render() !!}
  @else
    {!! $player['tweets']->render() !!}
  @endif
</div>
@stop

@section('stylesheet')
@stop

@section('javascript')
@stop