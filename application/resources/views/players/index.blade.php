@extends('system-template')

@section('content')

<div class="panel mb25">
  <div class="panel-heading">
    <h2>Players</h2>
  </div>

  <div class="panel-body">
    <p>
      {{ $players->count() }} of {{ $players->total() }} players showing |
        show per page
        <a href="{{ Request::url() }}?count=10">10</a>
        <a href="{{ Request::url() }}?count=25">25</a>
        <a href="{{ Request::url() }}?count=50">50</a>
    </p>

    @if (strpos(Request::getQueryString(),'count') !== false)
      {!! $players->appends(['count' => Input::get('count')])->render() !!}
    @else
      {!! $players->render() !!}
    @endif
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Team</th>
          <th>position</th>
          <th>Jersey</th>
      </thead>
      <tbody>
        @foreach($players as $player)
          <tr>
            <td><a href="{{ url('admin/fantasy/nfl-players/' . $player->id) }}">{{ $player->display_name }}</a></td>
            <td>{{ $player->full_name }}</td>
            <td>{{ $player->position }}</td>
            <td>{{ $player->jersey }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@stop

@section('stylesheet')
@stop

@section('javascript')
@stop
