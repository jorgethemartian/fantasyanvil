    <!-- main navigation -->
    <nav role="navigation">

      <ul class="nav">

        <!-- dashboard -->
        <li>
          <a href="{{ action('DashboardController@index') }}">
            <i class="fa fa-flask"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <!-- /dashboard -->

        <!-- ui -->
        <li>
          <a href="{{ action('TeamsController@index') }}">
            <i class="fa fa-users"></i>
            <span>NFL Teams</span>
          </a>
        </li>
        <li>
          <a href="{{ action('PlayersController@index') }}">
            <i class="fa fa-user"></i>
            <span>NFL Players</span>
          </a>
        </li>
        <li>
          <a href="{{ action('VocabulariesController@index') }}">
            <i class="fa fa-quote-left"></i>
            <span>Vocabulary</span>
          </a>
        </li>
        <!-- /ui -->
      </ul>
    </nav>
    <!-- /main navigation -->
