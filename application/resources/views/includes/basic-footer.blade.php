

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="/scripts/extentions/modernizr.js"></script>
  <script src="/vendor/jquery/dist/jquery.js"></script>
  <script src="/vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="/vendor/jquery.easing/jquery.easing.js"></script>
  <script src="/vendor/fastclick/lib/fastclick.js"></script>
  <script src="/vendor/onScreen/jquery.onscreen.js"></script>
  <script src="/vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="/scripts/ui/accordion.js"></script>
  <script src="/scripts/ui/animate.js"></script>
  <script src="/scripts/ui/link-transition.js"></script>
  <script src="/scripts/ui/panel-controls.js"></script>
  <script src="/scripts/ui/preloader.js"></script>
  <script src="/scripts/ui/toggle.js"></script>
  <script src="/scripts/urban-constants.js"></script>
  <script src="/scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  @yield('javascript')

</body>

</html>
