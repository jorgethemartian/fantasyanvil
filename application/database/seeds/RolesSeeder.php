<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->insert([
          'role' => 'authenticated',
          'created_at' => new DateTime,
          'updated_at' => new DateTime
        ]);
      DB::table('roles')->insert([
          'role' => 'administrator',
          'created_at' => new DateTime,
          'updated_at' => new DateTime
        ]);
    }
}
