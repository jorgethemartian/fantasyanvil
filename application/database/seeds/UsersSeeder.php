<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name' => 'Jorge Calderon',
        'email' => 'jorge@inventsimple.com',
        'password' => bcrypt('Redblue3'),
        'created_at' => new DateTime,
        'updated_at' => new DateTime
      ]);
    }
}
