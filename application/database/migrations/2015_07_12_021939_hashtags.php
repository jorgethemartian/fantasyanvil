<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hashtags extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('hashtags', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('player_tweets_id')->unsigned();
      $table->string('hashtags', 150);
      $table->dateTime('created_at');

      $table->foreign('player_tweets_id')
        ->references('id')
        ->on('player_tweets');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('hashtags');
  }
}
