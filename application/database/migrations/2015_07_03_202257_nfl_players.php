<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NflPlayers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('nfl_players', function (Blueprint $table) {
        $table->increments('id');
        $table->boolean('active');
        $table->integer('team_id')->unsigned();
        $table->integer('jersey')->unsigned();
        $table->string('first_name', 40);
        $table->string('last_name', 40);
        $table->string('display_name', 100);
        $table->string('position', 5);
        $table->string('height', 5);
        $table->integer('weight')->unsigned();
        $table->date('dob');
        $table->string('college', 100);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('nfl_players');
    }
}
