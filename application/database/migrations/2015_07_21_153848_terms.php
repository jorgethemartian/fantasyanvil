<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Terms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('terms', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('vocabulary_id')->unsigned();
        $table->integer('parent_id')->unsigned();
        $table->string('term', 150);
        $table->integer('weight');
        $table->timestamps();

        $table->foreign('vocabulary_id')
          ->references('id')
          ->on('vocabularies');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('terms');
    }
}
