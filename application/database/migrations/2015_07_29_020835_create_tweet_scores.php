<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetScores extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('tweet_scores', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('player_tweets_id')->unsigned();
      $table->integer('score');
      $table->date('created_at');

      $table->foreign('player_tweets_id')
        ->references('id')
        ->on('player_tweets');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('tweet_scores');
  }
}
