<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlayerTweets extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('player_tweets', function (Blueprint $table) {
      $table->engine = 'InnoDB';
      $table->increments('id');
      $table->integer('player_id')->unsigned();
      $table->string('text', 175);
      $table->string('source', 255);
      $table->string('location', 100);
      $table->string('twitter_screen_name', 16);
      $table->dateTime('tweet_created_at');
      $table->dateTime('created_at');

      $table->foreign('player_id')
        ->references('id')
        ->on('nfl_players');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('player_tweets');
  }
}
