<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TweetUrls extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('tweet_urls', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('player_tweets_id')->unsigned();
      $table->string('urls', 255);
      $table->dateTime('created_at');

      $table->foreign('player_tweets_id')
        ->references('id')
        ->on('player_tweets');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('tweet_urls');
  }
}
