<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Thujohn\Twitter\Facades\Twitter;

class Tweets extends Model
{
  /**
   * Declare database table columns for mass assignment
   *
   * @var array
   */
  protected $fillable = [
    'player_id',
    'text',
    'source',
    'location',
    'twitter_screen_name',
    'tweet_created_at',
    'created_at'
  ];

  protected $dates = ['tweet_created_at'];

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'player_tweets';

  /**
   * Disable timestamps for this database table.
   *
   * @var boolean
   */
  public $timestamps = false;

  /**
   * Get the screen name and linkify it.
   *
   * @param  string  $value
   * @return string
   */
  public function getTwitterScreenNameAttribute($user)
  {
    return Twitter::linkify('@' . $user);
  }

  /**
   * Get the tweet and linkify it.
   *
   * @param  string  $value
   * @return string
   */
  public function getTextAttribute($text)
  {
    return Twitter::linkify($text);
  }

  /**
   * Get the twitter created_at date and format it.
   *
   * @param  string  $value
   * @return string
   */
  public function getTweetCreatedAtAttribute($date)
  {
    return Carbon::parse($date)->setTimezone('America/New_York')->format('F d Y, g:i A');
  }

  /**
   * Get the hashtags for this tweet.
   */
  public function scopeHashtags()
  {
    return $this->hasMany('App\Hashtags', 'player_tweets_id');
  }

  /**
   * Get the mentions for this tweet.
   */
  public function scopeMentions()
  {
    return $this->hasMany('App\Mentions', 'player_tweets_id');
  }
}
