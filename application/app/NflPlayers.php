<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Inventsimple\FantasyFootballNerd;
use \FootballApi;

class NflPlayers extends Model
{
  /**
   * Declare database table columns for mass assignment
   *
   * @var array
   */
  protected $fillable = [
    'active',
    'team_id',
    'jersey',
    'first_name',
    'last_name',
    'display_name',
    'position',
    'height',
    'weight',
    'dob',
    'college',
    'created_at',
    'updated_at',
  ];

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'nfl_players';

  /**
   * Save players to database
   *
   * @return Response
   */
  public static function savePlayers()
  {
    // call football api to get all teams
    $players = FootballApi::players();

    // set data array
    $data = array();

    foreach ($players as $key => $player)
    {
      // check for already existing data
      $check = NflPlayers::where('display_name', $player->displayName)->first();

      // if no data exists added to data array
      if (is_null($check))
      {
        // get the team id
        $teamId = NflTeams::where('code', $player->team)->pluck('id');

        $data[$key] = array(
          'active' => (int) $player->active,
          'team_id' => (int) $teamId,
          'jersey' => (int) $player->jersey,
          'first_name' => $player->fname,
          'last_name' => $player->lname,
          'display_name' => $player->displayName,
          'position' => $player->position,
          'height' => $player->height,
          'weight' => $player->weight,
          'dob' => $player->dob,
          'college' => $player->college,
          'created_at' => new \DateTime,
          'updated_at' => new \DateTime
        );

        NflPlayers::insert($data[$key]);
      }
    }

    // if data exist, save it
    if (count($data) > 0)
    {
      // return data array
      return $data;
    }

    // if no data exists, return null
    return null;
  }
}
