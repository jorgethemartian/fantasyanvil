<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terms extends Model
{
  /**
   * Declare database table columns for mass assignment
   *
   * @var array
   */
  protected $fillable = [
    'id',
    'vocabulary_id',
    'parent_id',
    'term',
    'weight',
    'created_at',
    'updated_at'
  ];

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'terms';

  /**
   * Get the vocabaularies this term belongs to.
   */
  public function scopeVocabularies()
  {
    return $this->belongsTo('App\Vocabularies', 'vocabulary_id');
  }
}
