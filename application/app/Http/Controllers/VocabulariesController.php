<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Vocabularies;

class VocabulariesController extends Controller
{
  /**
   * Request object
   */
  protected $request;

  /**
   * Constructor method
   *
   * @return void
   */
  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $vocabularies = array();

    // get querystring
    $querystring = $this->request->getQueryString();
    parse_str($querystring, $querystring);

    // set pagination count
    if (isset($querystring['count']))
    {
      $count = (int) $querystring['count'];
    } else {
      $count = (int) 25;
    }

    // get all players
    $vocabularies = Vocabularies::select('vocabularies.*')
        ->orderBy('updated_at', 'DESC')
        ->paginate($count);

    return view('taxonomies.vocabularies.index')
        ->with('vocabularies', $vocabularies);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $vocabulary = array(
      'name' => '',
    );
    $formType = 'Add Vocabulary';

    return view('taxonomies.vocabularies.edit')
        ->with('vocabulary', (object) $vocabulary)
        ->with('formType', $formType);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  Request  $request
   * @return Response
   */
  public function store(Request $request)
  {
    Vocabularies::create($request->all());

    return redirect('admin/taxonomy/vocabularies');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    // load the vocabulary by id
    $vocabulary = Vocabularies::select('id', 'vocabularies.vocabulary as name')
        ->where('id', $id)->first();

    $formType = 'Edit Vocabulary';

    return view('taxonomies.vocabularies.edit')
        ->with('vocabulary', $vocabulary)
        ->with('formType', $formType);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  Request  $request
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $vocabulary = Vocabularies::findOrFail($id);

    $vocabulary->update($request->all());

    return redirect('admin/taxonomy/vocabularies');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    return Vocabularies::where('id', $id)->delete();
  }
}
