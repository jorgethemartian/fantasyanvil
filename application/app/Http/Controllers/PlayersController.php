<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Tweets;
use App\NflTeams;
use App\NflPlayers;

use Carbon\Carbon;

class PlayersController extends Controller
{
  /**
   * Request object
   */
  protected $request;

  /**
   * Constructor method
   *
   * @return void
   */
  public function __construct(Request $request) {
    $this->request = $request;
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    return $this->team();
  }

  /**
   * Display a listing of the resource by teams.
   *
   * @return Response
   */
  public function team()
  {
    // get querystring
    $querystring = $this->request->getQueryString();
    parse_str($querystring, $querystring);

    // set pagination count
    if (isset($querystring['count']))
    {
      $count = (int) $querystring['count'];
    } else {
      $count = (int) 25;
    }

    // get the team from uri path
    $team = $this->request->path();
    $team = explode('/', $team);

    if (isset($team[3]))
    {
      $team = strtoupper($team[3]);

      // make sure team exists
      $teamId = NflTeams::where('code', $team)->pluck('id');
    }

    if (isset($teamId))
    {
      // get team players
      $players = NflPlayers::select('nfl_players.*', 'nfl_teams.full_name')
          ->join('nfl_teams', 'nfl_players.team_id', '=', 'nfl_teams.id')
          ->where('team_id', $teamId)
          ->where('active', 1)
          ->orderBy('position', 'asc')
          ->paginate($count);

    } else {
      // get all players
      $players = NflPlayers::select('nfl_players.*', 'nfl_teams.full_name')
          ->join('nfl_teams', 'nfl_players.team_id', '=', 'nfl_teams.id')
          ->where('active', 1)
          ->orderBy('position', 'asc')
          ->paginate($count);
    }

    // if no players are returned
    if (count($players) === 0) {
      // access api and get all players
      NflPlayers::savePlayers();

      // redirect back to self
      return redirect()->route($this->request->path());
    }

    // redirect to view with teams array
    return view('players.index')->with('players', $players);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
      //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    // get querystring
    $querystring = $this->request->getQueryString();
    parse_str($querystring, $querystring);

    // set pagination count
    if (isset($querystring['count']))
    {
      $count = (int) $querystring['count'];
    } else {
      $count = (int) 25;
    }
    $player = NflPlayers::select('display_name', 'team_id', 'position')
        ->where('id', $id)
        ->get();

    $tweets = Tweets::select('text', 'source', 'twitter_screen_name', 'tweet_created_at', 'created_at')
        ->where('player_id', '=', $id)
          ->orderBy('tweet_created_at', 'DESC')
          ->paginate($count);



    return view('players.show')->with('player', array('info' => $player, 'tweets' => $tweets));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      //
  }
}
