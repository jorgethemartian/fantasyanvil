<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Vocabularies;
use App\Terms;

class VocabulariesTermsController extends Controller
{
  /**
   * Request object
   */
  protected $request;

  /**
   * Constructor method
   *
   * @return void
   */
  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index($vocabulariesId)
  {
    $terms = array();

    // get querystring
    $querystring = $this->request->getQueryString();
    parse_str($querystring, $querystring);

    // set pagination count
    if (isset($querystring['count']))
    {
      $count = (int) $querystring['count'];
    } else {
      $count = (int) 25;
    }
    // get all players
    $terms = Terms::select('terms.*')
        ->where('vocabulary_id', $vocabulariesId)
        ->orderBy('term', 'asc')
        ->paginate($count);

    return view('taxonomies.terms.index')
        ->with('terms', $terms)
        ->with('vocabulariesId', $vocabulariesId);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create($vocabulariesId)
  {
    $term = array(
      'vocabulary_id' => $vocabulariesId,
      'term' => '',
    );

    $formType = 'Add Term';

    return view('taxonomies.terms.edit')
        ->with('term', (object) $term)
        ->with('formType', $formType);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  Request  $request
   * @return Response
   */
  public function store(Request $request)
  {
    if (!Terms::where('term', '=', $request->term)->exists()) {
      Terms::create($request->all());
    }

    return redirect('admin/taxonomy/vocabularies/' . $request->vocabulary_id . '/terms');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $termsId
   * @param  int  $vocabulariesId
   * @return Response
   */
  public function show($termsId, $vocabulariesId)
  {
      dd($vocabulariesId);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($vocabulariesId, $termsId)
  {
    // load the vocabulary by id
    $term = Terms::select('terms.id', 'terms.vocabulary_id', 'terms.term')
        ->where('id', $termsId)->first();

    $formType = 'Edit Term';

    return view('taxonomies.terms.edit')
        ->with('term', $term)
        ->with('vocabulary_id', $vocabulariesId)
        ->with('formType', $formType);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  Request  $request
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $vocabulariesId, $termsId)
  {
    $term = Terms::findOrFail($termsId);

    $term->update($request->all());

    return redirect('admin/taxonomy/vocabularies/' . $vocabulariesId . '/terms');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($vocabulariesId, $termsId)
  {
    return Terms::where('id', $termsId)->delete();
  }
}
