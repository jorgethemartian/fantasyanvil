<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\NflTeams;
use App\NflPlayers;
use App\Tweets;
use App\Hashtags;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Thujohn\Twitter\Facades\Twitter;
use App\Jobs\SearchTwitter;

class TweetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      //
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function teams()
    {
      //
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getTeams($teamId)
    {
      // dd(Twitter::getAppRateLimit());

      // get the player by id
      $players = NFlPlayers::select('id', 'team_id', 'display_name')
          ->where('active', 1)
          ->where('jersey', '>', 0)
          ->where('team_id', $teamId)
          ->get();

      // set the delay to 5 seconds
      $delay = 5;

      foreach ($players as $key => $player)
      {
        // queue a twitter search for the player given
        // delay the job by 2 seconds times players position on list
        $job = (new SearchTwitter($player))->delay((int) $delay * ($key + 1));

        // dispatch the job
        $this->dispatch($job);
      }

      return $players;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
      // dd(Twitter::getAppRateLimit());
      // get all players
      $players = NFlPlayers::select('id', 'team_id', 'display_name')
          ->where('active', 1)
          ->where('jersey', '>', 0)
          // ->where('team_id', $teamId)
          ->get();

      // set the delay to 5 seconds
      $delay = 5;

      foreach ($players as $key => $player)
      {
        // queue a twitter search for the player given
        // delay the job by 2 seconds times players position on list
        $job = (new SearchTwitter($player))->delay((int) $delay * ($key + 1));

        // dispatch the job
        $this->dispatch($job);
      }

      return $players;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
