<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
  return view('welcome');
});

// Fantasy football app routes
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
  Route::get('/fantasy/nfl-teams', 'TeamsController@index');
  Route::get('/fantasy/nfl-teams/{team?}/players', 'PlayersController@team')
    ->where(['name' => '[a-z]+']);
  Route::get('/fantasy/nfl-players', 'PlayersController@index');
  Route::get('/fantasy/nfl-players/{id}', 'PlayersController@show');
  Route::get('/tweets', 'TweetsController@index');
  Route::get('/tweets/nfl-players/get', 'TweetsController@create');
  Route::get('/tweets/nfl-players/{id}', 'TweetsController@show');
  Route::get('/tweets/nfl-teams/{id}', 'TweetsController@teams');
  Route::get('/tweets/nfl-teams/{id}/get', 'TweetsController@getTeams');
  Route::resource('/taxonomy/vocabularies', 'VocabulariesController');
  Route::resource('/taxonomy/vocabularies.terms', 'VocabulariesTermsController');
});

// Twitter Login Routes
Route::group(['prefix' => 'twitter', 'middleware' => 'auth'], function () {
  Route::get('login', [
    'as' => 'twitter.login',
    'uses' => 'TwitterController@login'
  ]);
  Route::get('callback', [
    'as' => 'twitter.callback',
    'uses' => 'TwitterController@callback'
  ]);
  Route::get('error', [
    'as' => 'twitter.error',
    'uses' => 'TwitterController@error'
  ]);
  Route::get('logout', [
    'as' => 'twitter.logout',
    'uses' => 'TwitterController@logout'
  ]);
});

// FantasyAnvil teams specific routes
Route::get('/dashboard', [
  'middleware' => 'auth',
  'as' => 'twitter.error',
  'uses' => 'DashboardController@index'
]);

// Authentication routes...
Route::get('/users/account/login', 'Auth\AuthController@getLogin');
Route::post('/users/account/login', 'Auth\AuthController@postLogin');
Route::get('/users/account/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('/users/account/register', 'Auth\AuthController@getRegister');
Route::post('/users/account/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('/users/account/recovery', 'Auth\PasswordController@getEmail');
Route::post('/users/account/recovery', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('/users/account/recovery/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('/users/account/recovery/reset', 'Auth\PasswordController@postReset');
