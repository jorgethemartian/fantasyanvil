<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Inventsimple\FantasyFootballNerd;
use \FootballApi;

class NflTeams extends Model
{
  /**
   * Declare database table columns for mass assignment
   *
   * @var array
   */
  protected $fillable = [
    'code',
    'full_name',
    'short_name'
  ];

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'nfl_teams';

  /**
   * Disable timestamps for this database table.
   *
   * @var boolean
   */
  public $timestamps = false;

  /**
   * Save teams to database
   *
   * @return Response
   */
  public static function saveTeams()
  {
    // call football api to get all teams
    $teams = FootballApi::teams();

    // set data array
    $data = array();

    foreach ($teams as $team)
    {
      // check for already existing data
      $check = NflTeams::where('code', $team->code)->first();

      // if no data exists added to data array
      if (is_null($check))
      {
        $data[] = array(
          'code' => $team->code,
          'full_name' => $team->fullName,
          'short_name' => $team->shortName
        );
      }
    }

    // if data exist, save it
    if (count($data) > 0)
    {
      NflTeams::insert($data);

      // return data array
      return $data;
    }

    // if no data exists, return null
    return null;
  }
}
