<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vocabularies extends Model
{
  /**
   * Declare database table columns for mass assignment
   *
   * @var array
   */
  protected $fillable = [
    'id',
    'vocabulary',
    'created_at',
    'updated_at'
  ];

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'vocabularies';

  /**
   * Get the terms for this vocabulary.
   */
  public function scopeTerms()
  {
    return $this->hasMany('App\Terms', 'vocabulary_id');
  }
}
