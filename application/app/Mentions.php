<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mentions extends Model
{
  /**
   * Declare database table columns for mass assignment
   *
   * @var array
   */
  protected $fillable = [
    'player_tweets_id',
    'mentions',
    'created_at'
  ];

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'mentions';

  /**
   * Disable timestamps for this database table.
   *
   * @var boolean
   */
  public $timestamps = false;

  /**
   * Get the tweet this mention belongs to.
   */
  public function tweets()
  {
    return $this->belongsTo('App\Tweets', 'player_tweets_id');
  }
}
