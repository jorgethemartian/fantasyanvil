<?php

namespace App\Jobs;

use Log;
use Carbon\Carbon;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Thujohn\Twitter\Facades\Twitter;
use App\Mentions;
use App\Hashtags;
use App\TweetUrls;
use App\TweetMedia;
use App\Tweets;

class SearchTwitter extends Job implements SelfHandling, ShouldQueue
{
  use InteractsWithQueue, SerializesModels;

  protected $player;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($player)
  {
    $this->player = $player;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    // get all tweets for the current player
    $tweets = Twitter::getSearch(
      array(
        'q' => '"' . $this->player->display_name . '"-filter:retweets',
        'result_type' => 'recent',
        'lang' => 'en',
        'count' => 100
      )
    );

    // if there are tweets save info
    if (count($tweets) > 0)
    {
      // save each tweet
      foreach ($tweets->statuses as $key => $status)
      {
        // check to see if tweet already exists
        $exists = Tweets::select('id')
            ->where('player_id', '=', $this->player->id)
            ->where('text', '=', $status->text)
            ->where('location', '=', $status->user->location)
            ->where('twitter_screen_name', '=', $status->user->screen_name)
            ->get();

        if ($exists->isEmpty())
        {
          if (!$status->retweeted)
          {
            // get the newly created id
            $tweetId = Tweets::insertGetId(
                [
                  'player_id' => $this->player->id,
                  'text' => $status->text,
                  'source' => $status->source,
                  'location' => $status->user->location,
                  'twitter_screen_name' => $status->user->screen_name,
                  'tweet_created_at' => gmdate('Y-m-d H:i:s', strtotime($status->created_at)),
                  'created_at' => Carbon::now()
                ]);

            // if user mentions exist, save them
            if (count($status->entities->user_mentions) > 0)
            {
              foreach ($status->entities->user_mentions as $key => $userMention)
              {
                Mentions::insert(
                    [
                      'player_tweets_id' => $tweetId,
                      'mentions' => $userMention->screen_name,
                      'created_at' => Carbon::now(),
                    ]);
              }
            }

            // if hashtags exist, save them
            if (count($status->entities->hashtags) > 0)
            {
              foreach ($status->entities->hashtags as $key => $hashtag)
              {
                Hashtags::insert(
                    [
                      'player_tweets_id' => $tweetId,
                      'hashtags' => $hashtag->text,
                      'created_at' => Carbon::now(),
                    ]);
              }
            }

            // if entities exist, save them
            if (count($status->entities->urls) > 0)
            {
              foreach ($status->entities->urls as $key => $url)
              {
                TweetUrls::insert(
                    [
                      'player_tweets_id' => $tweetId,
                      'urls' => $url->url,
                      'created_at' => Carbon::now(),
                    ]);
              }
            }
          }
        }
        else
        {
          // log tweet not saved
          Log::info('Tweet not saved for player: ' . $this->player->display_name);
        }
      }

      // log player tweets saved
      Log::info('Ran twitter search for player: ' . $this->player->display_name);
    }
  }
}
