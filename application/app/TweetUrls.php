<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TweetUrls extends Model
{
  /**
   * Declare database table columns for mass assignment
   *
   * @var array
   */
  protected $fillable = [
    'player_tweets_id',
    'urls',
    'created_at'
  ];

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'tweet_urls';

  /**
   * Disable timestamps for this database table.
   *
   * @var boolean
   */
  public $timestamps = false;

  /**
   * Get the tweet this hastag belongs to.
   */
  public function scopeTweets()
  {
    return $this->belongsTo('App\Tweets', 'player_tweets_id');
  }
}