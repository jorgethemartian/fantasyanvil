<?php

return array(
	'route_prefix' => 'admin',
  'layout' => [
    'extends' => 'system-template',
    'header' => 'header',
    'content' => 'content',
  ],
  'table' => [
    'name' => 'Vocabularies',
    'actions' => '',
    'button' => 'Edit vocabulary',
  ],
);
