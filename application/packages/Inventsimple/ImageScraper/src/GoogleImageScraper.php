<?php

namespace Inventsimple\ImageScraper;

use \Kurt\Google\Core;

class GoogleImageScraper
{
  /**
   * google service object
   */
  private $googleServicesCore;

  private $service;
  private $key;
  private $startDate;
  private $endDate;

  /**
   * Create a new Skeleton Instance
   */
  public function __construct(Core $googleServicesCore)
  {
    $this->googleServicesCore = $googleServicesCore;

    $this->setupConfiguration();

    $this->setupCustomSearchService();

    $this->setupDates();
  }

  /**
   * Set up API key
   *
   */
  private function setupConfiguration()
  {
    $this->key = $this->googleServicesCore->getSettings('apiKey');
  }

  private function setupDates()
  {
    $this->startDate = date('Y-m-d', strtotime('-1 month'));
    $this->endDate = date('Y-m-d');
  }

  /**
   * Set up Custom Search Service
   *
   */
  private function setupCustomSearchService()
  {
    // Create Google Custom Search Service object
    $this->service = new \Google_Service_Customsearch(
      $this->googleServicesCore->getClient()
    );
  }

  /**
   * Friendly welcome
   *
   * @param string $phrase Phrase to return
   *
   * @return string Returns the phrase passed in
   */
  public function echoPhrase($phrase)
  {
    $data = $this->service->cse->listCse('arian foster', array());
    dd($data);
    return $phrase;
  }
}
