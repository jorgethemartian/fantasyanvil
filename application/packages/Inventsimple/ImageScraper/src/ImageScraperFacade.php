<?php
namespace Inventsimple\ImageScraper;

use Illuminate\Support\Facades\Facade;

class ImageScraperFacade extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'GoogleImageScraper';
  }
}