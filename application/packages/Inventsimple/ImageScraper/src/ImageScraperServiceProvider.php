<?php

namespace Inventsimple\ImageScraper;

use \Kurt\Google\Core;

use Illuminate\Support\ServiceProvider;

class ImageScraperServiceProvider extends ServiceProvider
{
  /**
   * Perform post-registration booting of services.
   *
   * @return void
   */
  public function boot()
  {
      //
  }

  /**
   * Register any package services.
   *
   * @return void
   */
  public function register()
  {
    $this->app->singleton(GoogleImageScraper::class, function ()
    {
      return new GoogleImageScraper(new Core);
    });
  }
}