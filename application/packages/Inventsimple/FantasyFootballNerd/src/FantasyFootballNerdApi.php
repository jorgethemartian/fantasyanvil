<?php

namespace Inventsimple\FantasyFootballNerd;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class FantasyFootballNerdApi {

  private $config = array();

  /**
   * load default items for use with the class
   *
   * @param  void
   *
   * @return void
   */
  public function __construct()
  {
    // load config data into config array
    $this->config['url'] = config('fantasyfootballnerd.url');
    $this->config['format'] = config('fantasyfootballnerd.format');
    $this->config['key'] = config('fantasyfootballnerd.key');
  }

  /**
   * get teams in the league
   *
   * @param  void
   *
   * @return json object
   */
  public function teams()
  {
    $service = 'nfl-teams';

    $teams = $this->getData($service);

    // remove multidimentional array and conver it single depth
    $teams = $teams->NFLTeams;

    return $teams;
  }

  /**
   * get players in the league
   *
   * @param  void
   *
   * @return json object
   */
  public function players()
  {
    $service = 'players';

    $players = $this->getData($service);

    // remove multidimentional array and conver it single depth
    $players = $players->Players;

    return $players;
  }

  /**
   * Cosume API endpoint and return data.
   *
   * @param  $service  The type of service to get.
   *
   * @return array
   */
  private function getData($service)
  {
    $apiUrl = $this->buildApiUrl($service);

    $request = trim(file_get_contents($apiUrl), '"');

    $request = json_decode($request);

    return $request;
  }

  /**
   * Build the API url.
   *
   * @param  $service  The type of service to get.
   *
   * @return url string
   */
  private function buildApiUrl($service)
  {
    // build the url and return
    return $this->config['url'] . '/' . $service . '/' . $this->config['format'] . '/' . $this->config['key'];
  }

}