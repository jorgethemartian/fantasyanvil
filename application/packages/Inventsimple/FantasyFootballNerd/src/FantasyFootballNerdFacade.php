<?php
namespace Inventsimple\FantasyFootballNerd;
use Illuminate\Support\Facades\Facade;

class FantasyFootballNerdFacade extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'FantasyFootballNerdApi';
  }
}