<?php

namespace Inventsimple\FantasyFootballNerd;

use Illuminate\Support\ServiceProvider;

class FantasyFootballNerdServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      $this->publishes([
        __DIR__.'/config/fantasyfootballnerd.php' => config_path('fantasyfootballnerd.php'),
      ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind('FantasyFootballNerdApi', function()
      {
          return new FantasyFootballNerdApi();
      });
    }
}
