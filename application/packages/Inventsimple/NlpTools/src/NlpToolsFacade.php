<?php
namespace Inventsimple\NlpTools;

use Illuminate\Support\Facades\Facade;

class NlpToolsFacade extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'Nlp';
  }
}