<?php

namespace Inventsimple\NlpTools;

use Illuminate\Support\ServiceProvider;

class NlpToolsServiceProvider extends ServiceProvider
{
  /**
   * Perform post-registration booting of services.
   *
   * @return void
   */
  public function boot()
  {
      //
  }

  /**
   * Register any package services.
   *
   * @return void
   */
  public function register()
  {
    $this->app->bind('Nlp', function()
    {
      return new Nlp();
    });
  }
}