<?php

namespace Inventsimple\NlpTools;

require_once dirname(__FILE__)."/../vendor/autoload.php";

use NlpTools\Tokenizers\WhitespaceTokenizer;
use NlpTools\Models\FeatureBasedNB;
use NlpTools\Documents\TrainingSet;
use NlpTools\Documents\TokensDocument;
use NlpTools\FeatureFactories\DataAsFeatures;
use NlpTools\Classifiers\MultinomialNBClassifier;

class Nlp {

  /**
   * load default items for use with the class
   *
   * @param   void
   *
   * @return  void
   */
  public function __construct()
  {

  }

  public function test()
  {
  }
}
